# Copyright (C) 2020
# Jessica McBroom and Benjamin Paassen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from pathlib import Path
import pandas as pd

__author__ = 'Jessica McBroom and Benjamin Paassen'
__copyright__ = 'Copyright 2020, Jessica McBroom and Benjamin Paassen'
__license__ = 'GPLv3'
__Version__ = '1.0.0'
__maintainer__ = 'Jessica McBroom'
__email__  = 'jmcb6755@uni.sydney.edu.au'


data_folder = Path("data")

# load the data that contains student confidence info
answer_metadata_path = data_folder/ 'metadata' / 'answer_metadata_task_3_4.csv'
answer_metadata_df = pd.read_csv(answer_metadata_path)[["AnswerId", "Confidence"]].dropna()

# load the data that contains the question ids
primary_data_path = data_folder/ 'train_data' / 'train_task_3_4.csv'
primary_df = pd.read_csv(primary_data_path)[["QuestionId", "AnswerId"]]

# merge the datasets
merged_df = answer_metadata_df.merge(primary_df, on="AnswerId")

# get the average confidence for each question
average_conf_df = merged_df.groupby('QuestionId')[["Confidence"]].mean()
average_confidence = average_conf_df["Confidence"].mean()

# fill in any missing questions with the average confidence
results_df = pd.DataFrame({'QuestionId':range(0,1000), 'Confidence':average_confidence})
results_df.Confidence = average_conf_df.Confidence
results_df.fillna(average_conf_df["Confidence"].mean(), inplace=True)

# rank the questions based on Confidence
results_df["ranking"] = results_df["Confidence"].rank(method='first', ascending=False)
results_df[["QuestionId", "ranking"]].to_csv( "confidence_ranking.csv")
